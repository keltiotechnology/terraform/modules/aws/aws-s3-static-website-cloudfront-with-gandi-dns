/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Common Variables
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "domain_name" {
  type        = string
  description = "Domain name"
}

variable "build" {
  type        = string
  description = "The path to the folder where you built your website (absolute or relative)"
}
/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Variables for AWS CERTIFICATE
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "subject_alternative_names" {
  type        = list(string)
  description = "Alternative names"
}

/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Variables for DNS
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "dns_ttl" {
  type        = number
  description = "Ttl of the dns records"
  default     = 3600
}
/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Variables for S3 Bucket
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "acl" {
  type        = string
  default     = "public-read"
  description = "The canned ACL to apply"
}

variable "index_document" {
  type        = string
  default     = "index.html"
  description = "Amazon S3 returns this index document when requests are made to the root domain or any of the subfolders"
}

variable "error_document" {
  type        = string
  default     = "index.html"
  description = "An absolute path to the document to return in case of a 4XX error"
}
/* 
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Variables for Cloudfront Distribution
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
variable "distribution_enabled" {
  type        = bool
  description = "Whether the distribution is enabled to accept end user requests for content"
  default     = true
}

variable "ipv6_enabled" {
  type        = bool
  description = "Whether the IPv6 is enabled for the distribution"
  default     = true
}

variable "comment" {
  type        = string
  default     = "Managed by Terraform"
  description = "Comment for the origin access identity"
}

variable "default_root_object" {
  type        = string
  default     = "index.html"
  description = "Object that CloudFront return when requests the root URL"
}

variable "origin_protocol_policy" {
  type        = string
  description = "The origin protocol policy to apply to your origin. One of 'http-only', https-only, or match-viewer"
  default     = "http-only"
}

variable "http_port" {
  type        = string
  description = "The HTTP port the custom origin listens on"
  default     = "80"
}

variable "https_port" {
  type        = string
  description = "The HTTPS port the custom origin listens on"
  default     = "443"
}

variable "origin_ssl_protocols" {
  type        = list(string)
  description = "The SSL/TLS protocols that you want CloudFront to use when communicating with your origin over HTTPS. A list of one or more of SSLv3, 'TLSv1', 'TLSv1.1', and 'TLSv1.2'"
  default     = ["TLSv1.2"]
}

variable "geo_restriction_type" {
  type = string

  # e.g. "whitelist"
  default     = "none"
  description = "Method that use to restrict distribution of your content by country: `none`, `whitelist`, or `blacklist`"
}

variable "geo_restriction_locations" {
  type = list(string)

  # e.g. ["US", "CA", "GB", "DE"]
  default     = []
  description = "List of country codes for which  CloudFront either to distribute content (whitelist) or not distribute your content (blacklist)"
}

variable "cloudfront_allowed_methods" {
  type        = list(string)
  description = "Controls which HTTP methods CloudFront processes and forwards to your Amazon S3 bucket or your custom origin"
  default     = ["GET", "HEAD"]
}

variable "cloudfront_cached_methods" {
  type        = list(string)
  description = "Controls whether CloudFront caches the response to requests using the specified HTTP methods"
  default     = ["GET", "HEAD"]
}

variable "cloudfront_enable_origin_shield" {
  type        = bool
  description = "A flag that specifies whether Origin Shield is enabled."
  default     = false
}

variable "cloudfront_enable_origin_shield_region" {
  type        = string
  description = "The AWS Region for Origin Shield. To specify a region, use the region code, not the region name. For example, specify the US East (Ohio) region as us-east-2."
  default     = ""
}

variable "forward_cookies" {
  type        = string
  default     = "none"
  description = "Specifies whether you want CloudFront to forward all or no cookies to the origin. Can be 'all' or 'none'"
}

variable "viewer_protocol_policy" {
  type        = string
  description = "Use this element to specify the protocol that users can use to access the files in the origin specified by TargetOriginId when a request matches the path pattern in PathPattern. One of 'allow-all', 'https-only', or 'redirect-to-https'"
  default     = "redirect-to-https"
}

variable "default_ttl" {
  type        = number
  description = "The default amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request in the absence of an Cache-Control max-age or Expires header"
  default     = 3600
}

variable "min_ttl" {
  type        = number
  description = "The minimum amount of time that you want objects to stay in CloudFront caches before CloudFront queries your origin to see whether the object has been updated"
  default     = 0
}

variable "max_ttl" {
  type        = number
  description = "The maximum amount of time that an object is in a CloudFront cache before CloudFront forwards another request to your origin to determine whether the object has been updated"
  default     = 86400
}

variable "ssl_support_method" {
  type        = string
  description = "Specifies how you want CloudFront to serve HTTPS requests"
  default     = "sni-only"
}

variable "minimum_protocol_version" {
  type        = string
  description = "Cloudfront TLS minimum protocol version."
  default     = "TLSv1"
}
