terraform {
  required_version = ">= 0.13.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.0"
    }
    gandi = {
      source  = "psychopenguin/gandi"
      version = "2.0.0-rc3"
    }
    null = {
      source  = "hashicorp/null"
      version = ">= 2.0"
    }
  }
}
