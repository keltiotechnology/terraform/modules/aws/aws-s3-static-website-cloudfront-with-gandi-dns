provider "aws" {
  region = "us-east-1"
}
provider "gandi" {}

module "static_website_on_gandi" {
  source = "../../"

  build = "./website"

  domain_name               = "poc.united-metaverse-nation.org"
  subject_alternative_names = ["www.poc.united-metaverse-nation.org"]
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.0"
    }
    gandi = {
      source  = "psychopenguin/gandi"
      version = "2.0.0-rc3"
    }
  }
}
